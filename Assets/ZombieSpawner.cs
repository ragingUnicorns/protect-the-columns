﻿using UnityEngine;

public class ZombieSpawner : MonoBehaviour
{
    public GameObject zombies;

    public Transform[] spawnPositions;
    private float timePassed;
    private float pauseSpawn;
    private float spawnSpeed = 5.0f;

    // Start is called before the first frame update
    void Start()
    {
        pauseSpawn = spawnSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        timePassed = Time.realtimeSinceStartup;

        if (Mathf.Round(timePassed) % 10 == 0 && pauseSpawn < 0 && timePassed > 90)
        {
            SpawnNewZombie();
            pauseSpawn = spawnSpeed;
        }
        pauseSpawn -= (0.0138f * 30);
    }
    void SpawnNewZombie()
    {
        new WaitForSeconds(UnityEngine.Random.Range(0, 5));
        GameObject zombie = Instantiate(zombies, spawnPositions[UnityEngine.Random.Range(0, 21)]);
        zombie.transform.localPosition = Vector3.zero;
    }
}
