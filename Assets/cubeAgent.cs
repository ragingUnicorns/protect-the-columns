﻿using UnityEngine;
using UnityEngine.AI;

public class cubeAgent : MonoBehaviour
{
    [SerializeField]
    private Transform goal;
    // Start is called before the first frame update
    void Start()
    {
        var cubeeeeAgent = GetComponent<NavMeshAgent>();
        cubeeeeAgent.destination = goal.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
